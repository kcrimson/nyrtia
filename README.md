# Nyrtia (distributed job scheduler (over MongoDB))

## jobs hooks (in order to support retries and many executions of the same job)

It is not supported as of know but should be rather simple to implement in the future.

We can introduce two interfaces Retry which will be called when job executed with error to ask job if we can retry and
???? interface which will be called when job was finished succesfully and we want to reschedule it for next execution.

These interfaces could return Optional<Job>, if optional is empty we don't retry or reschedule.  

## worker failure and recovery (notes)

All workers are responsible for updating its state in a persistent storage on regular basis. Every x s, worker sends a heartbeat (updates timestamp). When it didn't update it, this node is considered failed (in next version, we could mark this node as suspected, and run a procedure
to check if this node maybe will resurrect from the dead)

Every worker in a cluster is responsible for checking periodically, if it's neighbors are well and alive. The algorithm is simple, 
every node fetches list of visible nodes in cluster, sorts by instance id, get's id of its neighbor and checks if it is alive.
If not it starts recovery procedure. 

Recovery procedure is simple, it takes all jobs reserved by dead node and moves them back to waiting states, and takes all the
running jobs and moves them to error state.

This algorithm assumes that worker accepts that somebody can still job it had reserved (TODO this needs to carefully tested)


## run simulator

	./mvnw package && (cd nyrtia-simulator && docker-compose build && docker-compose up )
	
## milestone 0

* implement discovery of worker failures and recovery of jobs
* implement simulator tests with MongoDB replica set, so we can check if it actually works the way it should
* implement simulator tests with randomly kill workers, and check if recovery works the way it should
* make sure that jobs arguments don't require to be annotated with @Embedded
* add gathering statistics from job execution