package io.nyrtia.scheduler;

import io.nyrtia.Worker;
import io.nyrtia.client.Client;
import io.nyrtia.client.ClientConfig;
import io.nyrtia.client.DefaultClient;
import io.nyrtia.config.ClusterConfig;
import io.nyrtia.executor.DefaultJobScheduler;
import io.nyrtia.factory.ReflectionScheduledJobFactory;
import io.nyrtia.store.MongoDBJobStore;
import io.nyrtia.worker.DefaultWorker;

public class Schedulers {

	public static Worker join(ClusterConfig clusterConfig) {

		Worker node = new DefaultWorker(new MongoDBJobStore(clusterConfig.jobStoreConfig()), new DefaultJobScheduler(), new ReflectionScheduledJobFactory(),clusterConfig);
		node.join();
		return node;

	}

	public static Client connect(ClientConfig clientConfig) {
		return new  DefaultClient(new MongoDBJobStore(clientConfig.jobStoreConfig()));
	}

}
