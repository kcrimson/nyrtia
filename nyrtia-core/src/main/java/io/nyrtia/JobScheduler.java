package io.nyrtia;

public interface JobScheduler {

	void scheduleJob(ScheduledJobRun jobRun);

	void shutdown();

}
