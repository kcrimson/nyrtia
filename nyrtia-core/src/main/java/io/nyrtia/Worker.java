package io.nyrtia;

import java.util.List;

import io.nyrtia.store.JobData;

public interface Worker {

	void join();

	JobId scheduleJob(JobData job);

	List<JobId> getJobIds(State state);

	void addJobListener(JobListener jobListener);

	void shutdown();

}
