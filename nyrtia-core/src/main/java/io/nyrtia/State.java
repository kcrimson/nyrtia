package io.nyrtia;

public enum State {

	/**
	 * job is waiting to be scheduled and run
	 */
	WAITING,
	/**
	 * one of the workers have reserved this job to be run
	 */
	RESERVED,
	/**
	 * job is running
	 */
	RUNNING,
	/**
	 * unrecoverable error, job will never be run again
	 */
	ERROR,
	/**
	 * no more runs will be scheduled
	 */
	FINISHED
	
}
