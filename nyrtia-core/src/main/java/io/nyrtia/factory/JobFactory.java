package io.nyrtia.factory;

import io.nyrtia.JobContext;
import io.nyrtia.ScheduledJob;

public interface JobFactory {

	Runnable jobInstance(ScheduledJob job, JobContext context) throws JobInstationException;

}
