package io.nyrtia.factory;

public class JobInstationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4208236577021513535L;

	public JobInstationException(Exception e) {
		super(e);
	}

}
