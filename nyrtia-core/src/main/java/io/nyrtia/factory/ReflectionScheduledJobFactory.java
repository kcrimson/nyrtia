package io.nyrtia.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

import io.nyrtia.JobContext;
import io.nyrtia.ScheduledJob;

public class ReflectionScheduledJobFactory implements JobFactory {

	@Override
	public Runnable jobInstance(ScheduledJob job, JobContext context) throws JobInstationException {

		Class<?> jobClass = job.getJobClass();
		Object args = job.getArgs();

		try {
			// lookup for all constructors
			List<Object> parametersBindings = null;
			Constructor<?>[] constructors = jobClass.getConstructors();
			for (Constructor<?> cons : constructors) {
				// bind constructor parameters to either args or context entry
				Parameter[] parameters = cons.getParameters();
				parametersBindings = new ArrayList<>();
				for (Parameter param : parameters) {
					
					if (isJobArg(args, param)) {
						parametersBindings.add(args);
						continue;
					}

					if(isJobContext(context, param)) {
						parametersBindings.add(contextValue(context, param));
						continue;						
					}
				}

				if (parametersBindings.size() == parameters.length) {
					return (Runnable)cons.newInstance(parametersBindings.toArray(new Object[] {}));
				}
				parametersBindings = null;
			}

			if (args == null) {
				// try to invoke default constructor
				return ((Runnable) jobClass.newInstance());
			}

			return (Runnable) jobClass.getConstructor(Object.class).newInstance(args);
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | SecurityException e) {
			throw new JobInstationException(e);

		}
	}

	private Object contextValue(JobContext context, Parameter param) {
		Context annotation = param.getAnnotation(Context.class);
		String contextKey = annotation.value();
		Object contextValue = context.get(contextKey);
		return contextValue;
	}

	private boolean isJobContext(JobContext context, Parameter param) {
		if (param.isAnnotationPresent(Context.class)) {
			Context annotation = param.getAnnotation(Context.class);
			String contextKey = annotation.value();
			if (context.contains(contextKey)) {
				Object contextValue = context.get(contextKey);
				if (contextValue.getClass().isAssignableFrom(param.getType())) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isJobArg(Object args, Parameter param) {
		return !param.isAnnotationPresent(Context.class)
				&& args.getClass().isAssignableFrom(param.getType());
	}

}
