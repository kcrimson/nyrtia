package io.nyrtia;

public interface JobContext {

	boolean contains(String contextKey);

	Object get(String contextKey);

}
