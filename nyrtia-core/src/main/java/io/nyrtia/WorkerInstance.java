package io.nyrtia;

import java.time.LocalDateTime;

public interface WorkerInstance {
	
	String getInstanceId();
	
	LocalDateTime getNextHeartbeat();

}
