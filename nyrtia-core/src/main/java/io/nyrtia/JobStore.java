package io.nyrtia;

import java.util.List;
import java.util.Optional;

import io.nyrtia.store.JobData;

public interface JobStore {

	// query store for jobs
	JobId save(JobData job);
	List<JobId> getJobIds(State state);

	// manage cluster membership state
	void leave(String instanceId);
	void hearbeat(String instanceId);
	void join(String instanceId);

	// fetch next jobs to be run
	ScheduledJobsIterator fetchJobs(String instanceId);

	
	// manage job state
	boolean tryRun(JobData jobData);
	Optional<JobData> tryReserve(JobData jobData, String instanceId);
	boolean tryFinish(JobData jobData);
	boolean tryError(JobData jobData, Throwable e);
	
	
	List<WorkerInstance> allInstances();

}
