package io.nyrtia;

import java.util.Iterator;

public interface ScheduledJobsIterator extends Iterator<ScheduledJob>{

	int count();
	
}
