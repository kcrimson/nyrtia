package io.nyrtia.executor;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.nyrtia.JobScheduler;
import io.nyrtia.ScheduledJobRun;

public class DefaultJobScheduler implements JobScheduler{

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultJobScheduler.class);
	
	private final ScheduledExecutorService jobsExecutor = Executors.newScheduledThreadPool(4);

	@Override
	public void scheduleJob(ScheduledJobRun jobRun) {
		// job reserved, try to schedule it
		LocalDateTime now = LocalDateTime.now();
		if (jobRun.getTrigger().getNextFireTime().isAfter(now)) {
			long runInSeconds = ChronoUnit.SECONDS.between(jobRun.getTrigger().getNextFireTime(), now);
			LOGGER.debug("scheduled job {} to be run in {}", jobRun, runInSeconds);

			// TODO what if we have enough things scheduled, we need pause firing
			// new jobs

			jobsExecutor.schedule(jobRun::run, runInSeconds, TimeUnit.SECONDS);
		} else {
			// TODO how to handle misfired jobs
			LOGGER.info("job {} was misfired", jobRun);
			jobRun.misfired();
		}
	}

	@Override
	public void shutdown() {
		jobsExecutor.shutdown();
		
		try {
			jobsExecutor.awaitTermination(4, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			LOGGER.error("job executor shutdown interrupted",e);
		}
	}

}
