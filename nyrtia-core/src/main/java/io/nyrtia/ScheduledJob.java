package io.nyrtia;

import java.util.Optional;

public interface ScheduledJob extends Job{
	
	String getReservedBy();

	State getState();
	
	Optional<ScheduledJob> tryReserve();

	boolean tryRun();

	boolean tryFinish();

	boolean tryError(Throwable e);

}
