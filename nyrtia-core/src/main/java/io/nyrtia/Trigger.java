package io.nyrtia;

import java.time.LocalDateTime;

public interface Trigger {
	
	public LocalDateTime getNextFireTime();

}
