package io.nyrtia;

import java.time.LocalDateTime;
import java.util.UUID;

public interface Job {

	Class<?> getJobClass();

	Trigger getTrigger();

	String getId();

	Object getArgs();

	public static class Builder {

		private Class<?> jobClass;
		private Object args;
		private LocalDateTime nextFireTime;
		private String id = UUID.randomUUID().toString();

		public Builder withJobClass(Class<?> jobClass) {
			this.jobClass = jobClass;
			return this;
		}

		public Builder withArgs(Object args) {
			this.args = args;
			return this;
		}

		public Builder withNextFireTime(LocalDateTime nextFireTime) {
			this.nextFireTime = nextFireTime;
			return this;
		}
		
		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Job build() {
			return new Job() {

				@Override
				public Class<?> getJobClass() {
					return jobClass;
				}

				@Override
				public Trigger getTrigger() {
					return new Trigger() {

						@Override
						public LocalDateTime getNextFireTime() {
							return nextFireTime;
						}};
				}

				@Override
				public String getId() {
					return id;
				}

				@Override
				public Object getArgs() {
					return args;
				}};
		}

	}


	public static Builder newJob() {
		return new Builder();
	}

}
