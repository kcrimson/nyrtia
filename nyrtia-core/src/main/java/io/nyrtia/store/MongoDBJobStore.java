package io.nyrtia.store;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.mongodb.morphia.query.Sort.ascending;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.FindAndModifyOptions;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.FindOptions;
import org.mongodb.morphia.query.MorphiaIterator;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

import com.mongodb.MongoClient;

import io.nyrtia.Job;
import io.nyrtia.JobId;
import io.nyrtia.JobStore;
import io.nyrtia.ScheduledJobsIterator;
import io.nyrtia.State;
import io.nyrtia.WorkerInstance;

public class MongoDBJobStore implements JobStore{

	private final Datastore ds;

	public MongoDBJobStore(MongoDBJobStoreConfig jobStoreConfig) {
		@SuppressWarnings("rawtypes")
		HashSet<Class> classes = new HashSet<>(Arrays.asList(NodeData.class, JobData.class));
		Morphia morphia = new Morphia(classes);
		Datastore ds = morphia.createDatastore(new MongoClient(jobStoreConfig.uri()), "sched");
		ds.ensureIndexes();
		this.ds = ds;
	}

	@Override
	public ScheduledJobsIterator fetchJobs(String instanceId) {
		
		Query<JobData> query = ds.find(JobData.class)
				.field("state").equal(State.WAITING)
				.order(ascending("trigger.nextFireTime"));
		
		MorphiaIterator<JobData,JobData> iterator = query.fetch(new FindOptions().limit(10));
		
		return new ScheduledJobsIteratorAdapter(this, instanceId, iterator);
	}

	@Override
	public Optional<JobData> tryReserve(JobData jobData,String instanceId) {
		Query<JobData> q = ds.createQuery(JobData.class);
		UpdateOperations<JobData> u = ds.createUpdateOperations(JobData.class);

		JobData data = ds.findAndModify(
			q.field("state").equal(State.WAITING)
			.field("_id").equal(jobData.getId()),
			u.set("state", State.RESERVED).set("reservedBy", instanceId),
			new FindAndModifyOptions().returnNew(true));
		
		return ofNullable(data);
		
	}


	@Override
	public boolean tryRun(JobData jobData) {
		Query<JobData> q = ds.createQuery(JobData.class);
		UpdateOperations<JobData> u = ds.createUpdateOperations(JobData.class);
		
		// TODO should we reload data here? and return new data once job in running state
		
		UpdateResults update = ds.update(
				q.field("state").equal(State.RESERVED)
				.field("reservedBy").equal(jobData.getReservedBy())
				.field("_id").equal(jobData.getId()),
				u.set("state", State.RUNNING));
		
		return update.getUpdatedCount() == 1;
	}
	
	@Override
	public boolean tryFinish(JobData jobData) {
		Query<JobData> q = ds.createQuery(JobData.class);
		UpdateResults u = ds.update(
				q.field("_id").equal(jobData.getId())
				.field("state").equal(State.RUNNING)
				.field("reservedBy").equal(jobData.getReservedBy()),
				ds.createUpdateOperations(JobData.class).set("state", State.FINISHED));
		return u.getUpdatedCount() == 1;
	}

	@Override
	public void join(String instanceId) {
		ds.save(new NodeData(instanceId, LocalDateTime.now().plus(20L, ChronoUnit.SECONDS)));
	}

	@Override
	public void leave(String instanceId) {
		ds.delete(ds.createQuery(NodeData.class).field("_id").equal(instanceId));
	}

	@Override
	public JobId save(JobData job) {
		return new JobId(ds.save(job).getId());		
	}

	@Override
	public void hearbeat(String instanceId) {
		UpdateOperations<NodeData> u = ds.createUpdateOperations(NodeData.class);
		ds.update(new Key<NodeData>(NodeData.class, "nodes", instanceId),
				u.set("nextHeartbeat", LocalDateTime.now().plus(20L, ChronoUnit.SECONDS)));		
	}
	
	

	@Override
	public List<WorkerInstance> allInstances() {
		throw new UnsupportedOperationException("allInstances");
	}

	@Override
	public List<JobId> getJobIds(State state) {
		return ds
				.createQuery(JobData.class)
				.filter("state", state)
				.asKeyList()
				.stream()
				.map(Key::getId)
				.map(JobId::new)
				.collect(toList());
	}

	@Override
	public boolean tryError(JobData jobData,Throwable e) {
		Query<JobData> q = ds.createQuery(JobData.class);
		UpdateResults u = ds.update(q.field("state").equal(State.RUNNING)
				// TODO and have reserved this job
				.field("_id").equal(jobData.getId()),
				ds.createUpdateOperations(JobData.class).set("state", State.ERROR));
		return u.getUpdatedCount() == 1;
	}

	public JobId save(Job job) {
		return save(new JobData(job.getJobClass(),job.getId(),new TriggerData(job.getTrigger().getNextFireTime()),job.getArgs()));
	}

}
