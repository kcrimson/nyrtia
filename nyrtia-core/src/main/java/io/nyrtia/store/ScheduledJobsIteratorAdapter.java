package io.nyrtia.store;

import org.mongodb.morphia.query.MorphiaIterator;

import io.nyrtia.ScheduledJob;
import io.nyrtia.ScheduledJobsIterator;

final class ScheduledJobsIteratorAdapter implements ScheduledJobsIterator {
	/**
	 * 
	 */
	private final MongoDBJobStore mongoDBJobStore;
	private final String instanceId;
	private final MorphiaIterator<JobData, JobData> iterator;

	ScheduledJobsIteratorAdapter(MongoDBJobStore mongoDBJobStore, String instanceId, MorphiaIterator<JobData, JobData> iterator) {
		this.mongoDBJobStore = mongoDBJobStore;
		this.instanceId = instanceId;
		this.iterator = iterator;
	}

	@Override
	public ScheduledJob next() {
		return new ScheduledJobAdapter(this.mongoDBJobStore, iterator.next(),instanceId);
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

	@Override
	public int count() {
		return iterator.getCursor().count();
	}
}