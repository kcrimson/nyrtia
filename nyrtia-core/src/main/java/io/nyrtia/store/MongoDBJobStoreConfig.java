package io.nyrtia.store;

import com.mongodb.MongoClientURI;

public class MongoDBJobStoreConfig {
	
	private String hostname = "localhost";
	private int port = 27017;
	private String uri = String.format("mongodb://%s:%d", hostname,port);
		
	public MongoDBJobStoreConfig withHostname(String hostname) {
		this.hostname = hostname;
		this.uri = String.format("mongodb://%s:%d", hostname,port);
		return this;
	}

	public MongoDBJobStoreConfig withPort(int port) {
		this.port = port;
		this.uri = String.format("mongodb://%s:%d", hostname,port);
		return this;
	}

	public MongoDBJobStoreConfig withUri(String uri) {
		this.uri = uri;
		return this;
	}

	public MongoClientURI uri() {
		return new MongoClientURI(uri);
	}
	
	
}
