package io.nyrtia.store;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

import io.nyrtia.State;

@Entity("jobs")
@Indexes({
	@Index(fields = {@Field("_id"),@Field("state")})
})
public class JobData {

	@Id
	private String _id;
	private TriggerData trigger;
	private State state;
	private String reservedBy;
	private Class<?> jobClass;
	private Object args;

	public JobData(Class<?> jobClass, String id, TriggerData trigger,Object args) {
		this.jobClass = jobClass;
		this._id = id;
		this.trigger = trigger;
		this.args = args;
		this.state = State.WAITING;
	}

	public JobData() {
	}

	public String getId() {
		return _id;
	}

	public TriggerData getTrigger() {
		return trigger;
	}

	public State getState() {
		return state;
	}

	public String getReservedBy() {
		return reservedBy;
	}

	public Class<?> getJobClass() {
		return jobClass;
	}

	@Override
	public String toString() {
		return String.format("JobData [_id=%s, trigger=%s, state=%s]", _id, trigger, state);
	}

	public Object getArgs() {
		return args;
	}

}
