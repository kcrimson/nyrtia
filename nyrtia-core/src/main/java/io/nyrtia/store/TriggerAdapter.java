package io.nyrtia.store;

import java.time.LocalDateTime;

import io.nyrtia.Trigger;

final class TriggerAdapter implements Trigger {
	
	private final JobData jobData;

	TriggerAdapter(JobData jobData) {
		this.jobData = jobData;
	}

	@Override
	public LocalDateTime getNextFireTime() {
		return jobData.getTrigger().getNextFireTime();
	}
}