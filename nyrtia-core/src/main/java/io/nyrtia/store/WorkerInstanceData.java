package io.nyrtia.store;

import java.time.LocalDateTime;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("instances")
public class WorkerInstanceData {

	@Id
	private String instanceId;
	private LocalDateTime nextHeartbeat;
	
	public WorkerInstanceData() {
		super();
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public LocalDateTime getNextHeartbeat() {
		return nextHeartbeat;
	}

	public void setNextHeartbeat(LocalDateTime nextHeartbeat) {
		this.nextHeartbeat = nextHeartbeat;
	}
	
}
