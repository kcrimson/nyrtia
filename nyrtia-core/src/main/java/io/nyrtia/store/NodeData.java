package io.nyrtia.store;

import java.time.LocalDateTime;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("nodes")
class NodeData {

	@Id
	private String id;
	private LocalDateTime nextHeartbeat;
	
	public NodeData() {
		super();
	}

	public NodeData(String id, LocalDateTime nextHeartbeat) {
		super();
		this.id = id;
		this.nextHeartbeat = nextHeartbeat;
	}

	public String getId() {
		return id;
	}

	public LocalDateTime getNextHeartbeat() {
		return nextHeartbeat;
	}
	
}
