package io.nyrtia.store;

import java.util.Optional;

import io.nyrtia.JobStore;
import io.nyrtia.ScheduledJob;
import io.nyrtia.State;
import io.nyrtia.Trigger;

final class ScheduledJobAdapter implements ScheduledJob{
	
	private final JobStore jobStore;
	private final JobData jobData;
	private final String instanceId;
	private final Trigger trigger;
	
	ScheduledJobAdapter(JobStore jobStore, JobData jobData, String instanceId) {
		super();
		this.jobStore = jobStore;
		this.jobData = jobData;
		this.trigger = new TriggerAdapter(jobData);
		this.instanceId = instanceId;
	}

	@Override
	public Class<?> getJobClass() {
		return jobData.getJobClass();
	}

	@Override
	public String getReservedBy() {
		return jobData.getReservedBy();
	}

	@Override
	public State getState() {
		return jobData.getState();
	}

	@Override
	public Trigger getTrigger() {
		return trigger;
	}

	@Override
	public String getId() {
		return jobData.getId();
	}

	@Override
	public Optional<ScheduledJob> tryReserve() {
		return jobStore
				.tryReserve(jobData, instanceId)
				.map(this::newScheduledJob);
	}
	
	private ScheduledJob newScheduledJob(JobData jobData) {
		return new ScheduledJobAdapter(jobStore, jobData, instanceId);
	}

	@Override
	public boolean tryRun() {
		return jobStore.tryRun(jobData);
	}

	@Override
	public boolean tryFinish() {
		return jobStore.tryFinish(jobData);
	}

	@Override
	public boolean tryError(Throwable e) {
		return jobStore.tryError(jobData,e);
	}

	@Override
	public Object getArgs() {
		return jobData.getArgs();
	}	
	
}
