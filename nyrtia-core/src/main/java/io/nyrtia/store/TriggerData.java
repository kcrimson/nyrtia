package io.nyrtia.store;

import java.time.LocalDateTime;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class TriggerData {

	private LocalDateTime nextFireTime;
	
	private long firedCount;

	public TriggerData() {
	}

	public TriggerData(LocalDateTime plus) {
		nextFireTime = plus;
	}

	public LocalDateTime getNextFireTime() {
		return nextFireTime;
	}

	public long getFiredCount() {
		return firedCount;
	}

	@Override
	public String toString() {
		return String.format("TriggerData [nextFireTime=%s, firedCount=%s]", nextFireTime, firedCount);
	}
	
}
