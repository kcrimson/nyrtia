package io.nyrtia.config;

import java.util.concurrent.TimeUnit;

import io.nyrtia.store.MongoDBJobStoreConfig;

public class ClusterConfig {

	private long sheduleJobsDelayMilis = TimeUnit.MILLISECONDS.convert(4, TimeUnit.SECONDS);
	private MongoDBJobStoreConfig jobStoreConfig = new MongoDBJobStoreConfig();

	public ClusterConfig withJobStore(MongoDBJobStoreConfig jobStoreConfig) {
		this.jobStoreConfig = jobStoreConfig;
		return this;
	}

	public long sheduleJobsDelayMilis() {
		return sheduleJobsDelayMilis;
	}

	public MongoDBJobStoreConfig jobStoreConfig() {
		return jobStoreConfig;
	}

	public ClusterConfig withSheduleJobsDelay(int duration, TimeUnit timeunit) {
		sheduleJobsDelayMilis = TimeUnit.MILLISECONDS.convert(duration, timeunit);
		return this;
	}

}
