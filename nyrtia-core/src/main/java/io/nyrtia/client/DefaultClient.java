package io.nyrtia.client;

import io.nyrtia.Job;
import io.nyrtia.JobId;
import io.nyrtia.store.MongoDBJobStore;

public class DefaultClient implements Client {

	private MongoDBJobStore ds;

	public DefaultClient(MongoDBJobStore ds) {
		this.ds = ds;
	}

	@Override
	public JobId scheduleJob(Job job) {
		return ds.save(job);
	}

}
