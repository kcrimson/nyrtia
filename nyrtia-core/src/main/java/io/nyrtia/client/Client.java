package io.nyrtia.client;

import io.nyrtia.Job;
import io.nyrtia.JobId;

public interface Client {

	JobId scheduleJob(Job build);

}
