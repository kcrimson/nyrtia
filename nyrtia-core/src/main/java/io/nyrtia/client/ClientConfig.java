package io.nyrtia.client;

import io.nyrtia.store.MongoDBJobStoreConfig;

public class ClientConfig {

	private MongoDBJobStoreConfig jobStoreConfig;

	public ClientConfig withJobStoreConfig(MongoDBJobStoreConfig jobStoreConfig) {
		this.jobStoreConfig = jobStoreConfig;
		return this;
	}

	public MongoDBJobStoreConfig jobStoreConfig() {
		return jobStoreConfig;
	}

}
