package io.nyrtia;

public class NoOpJob implements Runnable {

	@Override
	public void run() {
		// this job does nothing
	}

}
