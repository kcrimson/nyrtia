package io.nyrtia.worker;

import io.nyrtia.ScheduledJob;
import io.nyrtia.ScheduledJobRun;
import io.nyrtia.Trigger;

final class ScheduledJobRunAdapter implements ScheduledJobRun {

	private final DefaultWorker defaultWorker;
	private final ScheduledJob job;

	ScheduledJobRunAdapter(DefaultWorker defaultWorker, ScheduledJob job) {
		this.defaultWorker = defaultWorker;
		this.job = job;
	}

	@Override
	public void run() {
		this.defaultWorker.runJob(job);						
	}

	@Override
	public Trigger getTrigger() {
		return job.getTrigger();
	}

	@Override
	public void misfired() {
		defaultWorker.jobMifired(job);
	}
}