package io.nyrtia.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.nyrtia.Job;
import io.nyrtia.JobContext;
import io.nyrtia.JobScheduler;
import io.nyrtia.JobId;
import io.nyrtia.JobListener;
import io.nyrtia.JobStore;
import io.nyrtia.ScheduledJob;
import io.nyrtia.ScheduledJobsIterator;
import io.nyrtia.State;
import io.nyrtia.Worker;
import io.nyrtia.config.ClusterConfig;
import io.nyrtia.factory.JobFactory;
import io.nyrtia.factory.JobInstationException;
import io.nyrtia.store.JobData;

public class DefaultWorker implements Worker {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultWorker.class);

	private final JobStore jobStore;
	private final ScheduledExecutorService heartbeatExecutor;
	private final ScheduledExecutorService schedulerExecutor;
	private final JobScheduler jobExecutor;
	private final JobFactory jobFactory;
	private final String instanceId;

	private final ClusterConfig clusterConfig;

	private final List<JobListener> listeners = new ArrayList<>();

	private JobContext context;


	public DefaultWorker(JobStore jobStore, JobScheduler jobExecutor, JobFactory jobFactory,ClusterConfig clusterConfig) {
		super();
		this.jobStore = jobStore;
		this.jobExecutor = jobExecutor;
		this.jobFactory = jobFactory;
		this.clusterConfig = clusterConfig;

		instanceId = UUID.randomUUID().toString();

		// keeps updating heartbeat
		this.heartbeatExecutor = Executors.newSingleThreadScheduledExecutor(
				new BasicThreadFactory.Builder().namingPattern("heartbeat-" + instanceId + "-%d").build());

		// keeps polling for jobs
		this.schedulerExecutor = Executors.newSingleThreadScheduledExecutor(
				new BasicThreadFactory.Builder().namingPattern("scheduler-" + instanceId + "-%d").build());

	}

	@Override
	public void join() {

		jobStore.join(instanceId);

		// schedule heart beat
		heartbeatExecutor.scheduleAtFixedRate(
				this::heartbeat, 
				10, 
				10, 
				TimeUnit.SECONDS);

		schedulerExecutor.scheduleAtFixedRate(
				this::fireJobs, 
				clusterConfig.sheduleJobsDelayMilis(), 
				clusterConfig.sheduleJobsDelayMilis(), 
				TimeUnit.MILLISECONDS);

	}

	@Override
	public void shutdown() {

		// stop scheduling new jobs

		schedulerExecutor.shutdown();

		try {
			schedulerExecutor.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// wait until running jobs finish

		jobExecutor.shutdown();		
		
		heartbeatExecutor.shutdown();
		
		try {
			heartbeatExecutor.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		jobStore.leave(instanceId);
		
		
	}

	@Override
	public JobId scheduleJob(JobData job) {
		return jobStore.save(job);
	}

	@Override
	public List<JobId> getJobIds(State state) {
		return jobStore.getJobIds(state);
	}

	private void heartbeat() {
		jobStore.hearbeat(instanceId);
	}

	private void fireJobs() {

		LOGGER.debug("looking for a jobs");

		ScheduledJobsIterator jobs = jobStore.fetchJobs(instanceId);

		LOGGER.debug("found next {} jobs for fire", jobs.count());

		while (jobs.hasNext()) {
			jobs.next()
				.tryReserve()
				.map(j -> new ScheduledJobRunAdapter(this, j))
				.ifPresent(jobExecutor::scheduleJob);
			
		}
	}


	void runJob(ScheduledJob job) {
			// TODO check if we are still eligible to be run, maybe somebody deleted us, so again
			// try to update state from RESERVED to RUNNING, or what's worse updated trigger,
			// so we need to compare nextFireTime
			LOGGER.debug("about to run job {} ", job);
			if (job.tryRun()) {
				// TODO should we refresh job state? I think we should
				// Once we are done, remove job, as don't support recurring jobs, as of now
				// or if finished with error, change state to ERROR (what about cleaning error
				// states)?

				try {
					jobFactory.jobInstance(job,context).run();
					job.tryFinish();
				} catch (JobInstationException e) {
					LOGGER.error("cannot instantiate job",e);
					job.tryError(e);
				} catch(Throwable e) {
					LOGGER.error("cannot run job",e);
					job.tryError(e);
				}

			} else {
				LOGGER.warn("job {} was deleted, stealed or updated", job);
				// TODO job was deleted in the meantime, 
				// or somebody stealed it from us, 
				// or was updated,
				// should we
				// care?
			}

	}

	@Override
	public void addJobListener(JobListener jobListener) {
		listeners.add(jobListener);
	}
	
	void jobMifired(Job job) {
		listeners.stream().forEach(l -> l.jobMisfired(job));
	}


}
