package io.nyrtia;

public interface ScheduledJobRun {

	Trigger getTrigger();
	
	void run();

	void misfired();

}
