package io.nyrtia;

import java.time.LocalDateTime;

class DetachedWorkerInstance implements WorkerInstance {

	private final LocalDateTime nextHeartbeat;
	private final String instanceId;

	DetachedWorkerInstance(LocalDateTime nextHeartbeat, String instanceId) {
		super();
		this.nextHeartbeat = nextHeartbeat;
		this.instanceId = instanceId;
	}

	@Override
	public String getInstanceId() {
		return instanceId;
	}

	@Override
	public LocalDateTime getNextHeartbeat() {
		return nextHeartbeat;
	}

}
