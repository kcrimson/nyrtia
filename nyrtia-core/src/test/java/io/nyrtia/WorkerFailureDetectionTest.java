package io.nyrtia;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;

public class WorkerFailureDetectionTest {

	@Test
	public void discover_failed_worker() {
		// given
		String instanceId = UUID.randomUUID().toString();
		String failedInstanceId = UUID.randomUUID().toString();
		
		JobStore jobStore = mock(JobStore.class);
		when(jobStore.allInstances())
			.thenReturn(asList(
					new DetachedWorkerInstance(LocalDateTime.now(), instanceId),
					new DetachedWorkerInstance(LocalDateTime.now().minusSeconds(1), failedInstanceId)
			));
		
		WorkerDeadDetector detector = new WorkerDeadDetector(instanceId, jobStore);
		// when
		String actualFailedInstanceId = detector.failedWorker();
		// then
		assertThat(actualFailedInstanceId).isEqualTo(failedInstanceId);
	}

}
