package io.nyrtia;

import java.time.LocalDateTime;

public class DetachedTrigger implements Trigger {

	private final LocalDateTime nextFireTime;

	public DetachedTrigger(LocalDateTime nextFireTime) {
		super();
		this.nextFireTime = nextFireTime;
	}

	@Override
	public LocalDateTime getNextFireTime() {
		return nextFireTime;
	}

}
