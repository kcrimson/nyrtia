package io.nyrtia;

import static java.util.Collections.unmodifiableCollection;

import java.util.Collection;
import java.util.Iterator;

class DetachedJobIterator implements ScheduledJobsIterator {

	private final Iterator<ScheduledJob> iterator;
	private final int size;

	DetachedJobIterator(Collection<ScheduledJob> jobs) {
		iterator = unmodifiableCollection(jobs).iterator();
		size = jobs.size();
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

	@Override
	public ScheduledJob next() {
		return iterator.next();
	}

	@Override
	public int count() {
		return size;
	}

}
