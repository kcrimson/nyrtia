package io.nyrtia;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public class DetachedScheduledJob implements ScheduledJob {


	private final String id = UUID.randomUUID().toString();
	private final boolean tryReserve;
	private final boolean tryRun;
	private final LocalDateTime nextFireTime;
	private final Class<?> jobClass;
	private final Object args;
	private final Runnable action;

	private boolean wasReserved;
	private boolean wasRun;
	private boolean wasFinished;

	public DetachedScheduledJob(boolean tryReserve,boolean tryRun, LocalDateTime nextFireTime, Class<?> jobClass, Object args, Runnable action) {
		this.tryReserve = tryReserve;
		this.tryRun = tryRun;
		this.nextFireTime = nextFireTime;
		this.jobClass = jobClass;
		this.args = args;
		this.action = action;
	}

	@Override
	public Class<?> getJobClass() {
		return jobClass;
	}
	
	@Override
	public Object getArgs() {
		return args;
	}

	@Override
	public String getReservedBy() {
		return null;
	}

	@Override
	public State getState() {
		return null;
	}

	@Override
	public Trigger getTrigger() {
		return new DetachedTrigger(nextFireTime);
	}

	@Override
	public String getId() {
		return id ;
	}

	@Override
	public Optional<ScheduledJob> tryReserve() {
		action.run();
		wasReserved = true;
		return tryReserve?Optional.of(this):Optional.empty();
	}

	@Override
	public boolean tryRun() {
		wasRun = true;
		return tryRun;
	}

	@Override
	public boolean tryFinish() {
		wasFinished = true;
		return true;
	}

	@Override
	public boolean tryError(Throwable e) {
		return false;
	}

	public boolean wasRun() {
		return wasRun;
	}

	public boolean wasReserved() {
		return wasReserved;
	}

	public boolean wasFinished() {
		return wasFinished;
	}

	public static Builder newJob() {
		return new Builder();
	}

	public static class Builder {

		private boolean tryReserve;
		private boolean tryRun;
		private LocalDateTime nextFireTime = LocalDateTime.now();
		private Class<?> jobClass=NoOpJob.class;
		private Object args;
		private Runnable action = () -> {};

		Builder tryReserve(boolean tryReserve) {
			this.tryReserve = tryReserve;
			return this;
		}
		
		public Builder tryRun(boolean b) {
			tryRun=true;
			return this;
		}

		public DetachedScheduledJob build() {
			return new DetachedScheduledJob(tryReserve,tryRun,nextFireTime,jobClass, args,action);
		}

		public Builder nextFireTime(LocalDateTime nextFireTime) {
			this.nextFireTime = nextFireTime;
			return this;
		}

		public Builder withJobClass(Class<?> jobClass) {
			this.jobClass = jobClass;
			return this;
		}

		public Builder withArgs(Object args) {
			this.args = args;
			return this;
		}

		public Builder onTryReserve(Runnable action) {
			this.action  = action;
			return this;
		}

	}

}
