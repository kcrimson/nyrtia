package io.nyrtia.factory;

import java.util.HashMap;
import java.util.Map;

import io.nyrtia.JobContext;

public class MapJobContext implements JobContext {

	private final Map<String, Object> map = new HashMap<>();

	public MapJobContext(String key, Object value) {
		map.put(key, value);
	}

	public MapJobContext() {
	}

	@Override
	public boolean contains(String contextKey) {
		return map.containsKey(contextKey);
	}

	@Override
	public Object get(String contextKey) {
		return map.get(contextKey);
	}

}
