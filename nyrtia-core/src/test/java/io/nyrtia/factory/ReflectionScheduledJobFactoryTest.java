package io.nyrtia.factory;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import io.nyrtia.DetachedScheduledJob;

public class ReflectionScheduledJobFactoryTest {

	@Test
	public void new_instance_with_args() throws Exception {
		JobFactory jobFactory = new ReflectionScheduledJobFactory();

		MapJobContext jobContext = new MapJobContext();

		Object args = new Object();
		WithArgsJob job = (WithArgsJob) jobFactory.jobInstance(
						DetachedScheduledJob
						.newJob()
						.withJobClass(WithArgsJob.class)
						.withArgs(args)
						.build(),
						jobContext
				);

		assertThat(job.getArgs()).isEqualTo(args);

	}

	@Test
	public void new_instance_with_context() throws Exception {
		JobFactory jobFactory = new ReflectionScheduledJobFactory();

		MapJobContext jobContext = new MapJobContext("context-string","hello");
		
		WithContextJob job = (WithContextJob) jobFactory.jobInstance(
						DetachedScheduledJob
						.newJob()
						.withJobClass(WithContextJob.class)
						.build(), 
						jobContext
				);

		assertThat(job.getContext()).isEqualTo("hello");

	}

	public static class WithArgsJob implements Runnable {

		private final Object args;

		public WithArgsJob(Object args) {
			this.args = args;
		}

		@Override
		public void run() {
		}

		Object getArgs() {
			return args;
		}

	}

	public static class WithContextJob implements Runnable {

		private final String context;

		public WithContextJob(@Context("context-string") String context) {
			this.context = context;
		}

		@Override
		public void run() {
		}

		String getContext() {
			return context;
		}

	}
}
