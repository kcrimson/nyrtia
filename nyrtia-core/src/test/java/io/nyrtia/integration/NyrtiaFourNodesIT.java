package io.nyrtia.integration;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.distribution.Version;
import io.nyrtia.JobId;
import io.nyrtia.State;
import io.nyrtia.Worker;
import io.nyrtia.config.ClusterConfig;
import io.nyrtia.scheduler.Schedulers;
import io.nyrtia.store.JobData;
import io.nyrtia.store.MongoDBJobStoreConfig;
import io.nyrtia.store.TriggerData;


public class NyrtiaFourNodesIT {
	
	static AtomicInteger atomic;
	
	private static final MongodStarter mongodStarter = MongodStarter.getDefaultInstance();

	private static MongodProcess mongodProcess;

	private static List<Worker> workers;
	
	@BeforeAll
	public static void beforeClass() throws Exception {
		
		IMongodConfig mongodConfig = new MongodConfigBuilder()
				.version(Version.Main.PRODUCTION)
				.build();
		
		MongodExecutable mongodExecutable = mongodStarter.prepare(mongodConfig);
		
		mongodProcess = mongodExecutable.start();
		int port = mongodProcess.getConfig().net().getPort();
		ClusterConfig clusterConfig = new ClusterConfig()
				.withJobStore(
						new MongoDBJobStoreConfig().withPort(port)
				)
				.withSheduleJobsDelay(100, TimeUnit.MILLISECONDS);
		
		// start cluster 
		workers = range(0, 32)
				.mapToObj( i-> Schedulers.join(clusterConfig))
				.collect(toList());
	}
	
	@AfterAll
	public static void afterClass() {
		
		workers.stream().forEach(Worker::shutdown);
		
		mongodProcess.stop();
	}	
	
	@Test
	public void finish_scheduled_tasks() throws InterruptedException, ExecutionException, TimeoutException {
		
		atomic = new AtomicInteger(0);
		
		Worker worker = workers.stream().findAny().get();
		
		List<JobId> jobIds = scheduleJobs(worker, 3200, IncrementCounter.class);
				
		await().atMost(60,TimeUnit.SECONDS).untilAtomic(atomic, equalTo(3200));
		
		List<JobId> finished = worker.getJobIds(State.FINISHED);
		
		assertThat(finished).containsOnlyElementsOf(jobIds);
		
	}
	
	@Test
	public void error_scheduled_tasks() throws InterruptedException, ExecutionException, TimeoutException {
		
		atomic = new AtomicInteger(0);
		
		Worker worker = workers.stream().findAny().get();
		
		List<JobId> jobIds = scheduleJobs(worker, 1600, IncrementCounterAndFail.class);
			
		scheduleJobs(worker, 1600, IncrementCounter.class);
						
		await().atMost(60,TimeUnit.SECONDS).untilAtomic(atomic, equalTo(3200));
		
		List<JobId> finished = worker.getJobIds(State.ERROR);
		
		assertThat(finished).containsOnlyElementsOf(jobIds);
		
	}

	private static List<JobId> scheduleJobs(Worker worker, int jobsCount, Class<?> jobClass) {
		
		ArrayList<JobId> ids = new ArrayList<>();
		LocalDateTime nextFireTime = LocalDateTime.now();
		for(int i=0;i<jobsCount;i++) {
			nextFireTime = nextFireTime.plus(200,ChronoUnit.MILLIS);
			JobId jobId = worker.scheduleJob(
					new JobData(
							jobClass,
							UUID.randomUUID().toString(), 
							new TriggerData(nextFireTime),null));
			ids.add(jobId);
		}
		
		return ids ;
	}
	
	public static class IncrementCounter implements Runnable{

		@Override
		public void run() {
			atomic.incrementAndGet();
		}
		
	}

	public static class IncrementCounterAndFail implements Runnable{

		@Override
		public void run() {
			atomic.incrementAndGet();
			throw new RuntimeException("job has failed");
		}
		
	}
}
