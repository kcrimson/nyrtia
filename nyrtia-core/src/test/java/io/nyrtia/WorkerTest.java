package io.nyrtia;

import static java.util.Arrays.asList;
import static org.awaitility.Awaitility.await;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import io.nyrtia.config.ClusterConfig;
import io.nyrtia.executor.DefaultJobScheduler;
import io.nyrtia.factory.ReflectionScheduledJobFactory;
import io.nyrtia.store.MongoDBJobStore;
import io.nyrtia.worker.DefaultWorker;

public class WorkerTest {

	@Test
	public void dont_schedule_job_when_not_reserved() {
		//given
		DetachedScheduledJob job = DetachedScheduledJob
				.newJob()
				.tryReserve(false)
				.nextFireTime(LocalDateTime.now().plus(2,ChronoUnit.SECONDS))
				.build();
		JobStore jobStore = mock(MongoDBJobStore.class);
		DefaultJobScheduler jobScheduler = mock(DefaultJobScheduler.class);
		
		//when
		when(jobStore.fetchJobs(anyString())).thenReturn(new DetachedJobIterator(asList(job)));
		
		Worker node = new DefaultWorker(
				jobStore, 
				jobScheduler, 
				new ReflectionScheduledJobFactory(), 
				new ClusterConfig().
				withSheduleJobsDelay(1, TimeUnit.SECONDS));
		node.join();
		
		//then
		await().atMost(4,TimeUnit.SECONDS).until(() -> job.wasReserved());
		verify(jobScheduler,never()).scheduleJob(any());
	}

	@Test
	public void job_was_run_and_finished() {
		//given
		DetachedScheduledJob job = DetachedScheduledJob
				.newJob()
				.tryReserve(true)
				.tryRun(true)
				.nextFireTime(LocalDateTime.now().plus(2,ChronoUnit.SECONDS))
				.build();
		
		JobStore jobStore = mock(MongoDBJobStore.class);
		DefaultJobScheduler jobScheduler = new DefaultJobScheduler();
		
		//when
		when(jobStore.fetchJobs(anyString())).thenReturn(new DetachedJobIterator(asList(job)));
		
		Worker node = new DefaultWorker(
				jobStore, 
				jobScheduler, 
				new ReflectionScheduledJobFactory(), 
				new ClusterConfig().withSheduleJobsDelay(1,TimeUnit.SECONDS));
		node.join();
		
		//then
		await().atMost(4,TimeUnit.SECONDS).until(() -> job.wasReserved());
		await().atMost(4,TimeUnit.SECONDS).until(() -> job.wasRun());
		await().atMost(4,TimeUnit.SECONDS).until(() -> job.wasFinished());
	}

	@Test
	public void dont_run_job_if_misfired() {
		//given
		DetachedScheduledJob job = DetachedScheduledJob
				.newJob()
				.tryReserve(true)
				.tryRun(true)
				.nextFireTime(LocalDateTime.now().plus(1,ChronoUnit.SECONDS))
				.build();
		
		JobStore jobStore = mock(MongoDBJobStore.class);
		DefaultJobScheduler jobScheduler = new DefaultJobScheduler();
		
		//when
		when(jobStore.fetchJobs(anyString())).thenReturn(new DetachedJobIterator(asList(job)));
		
		Worker node = new DefaultWorker(
				jobStore, 
				jobScheduler, 
				new ReflectionScheduledJobFactory(), 
				new ClusterConfig().withSheduleJobsDelay(2,TimeUnit.SECONDS));
		node.join();
		
		JobListener jobListener = mock(JobListener.class);
		node.addJobListener(jobListener);
		
		//then
		await().atMost(4,TimeUnit.SECONDS).until(() -> job.wasReserved());
		verify(jobListener).jobMisfired(any(Job.class));

	}
	
}
