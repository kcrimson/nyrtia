package io.nyrtia;

import static java.util.Collections.sort;

import java.time.LocalDateTime;
import java.util.List;

class WorkerDeadDetector {

	private final String instanceId;
	private final JobStore jobStore;

	WorkerDeadDetector(String instanceId, JobStore jobStore) {
		this.instanceId = instanceId;
		this.jobStore = jobStore;
	}

	String failedWorker() {
		List<WorkerInstance> allInstances = jobStore.allInstances();
		sort(allInstances, (i1,i2) -> i1.getInstanceId().compareTo(i2.getInstanceId()));
		
		
		WorkerInstance thisWorkerInstance = allInstances
			.stream()
			.filter( w-> w.getInstanceId().equals(instanceId))
			.findFirst().get();
		
		int idx = allInstances.indexOf(thisWorkerInstance);
		
		int neighbourIdx = idx+1;
		
		if(neighbourIdx>allInstances.size()-1) {
			neighbourIdx=0;
		}
		
		WorkerInstance neighbourInstances = allInstances.get(neighbourIdx);
		
		if(neighbourInstances.getNextHeartbeat().isBefore(LocalDateTime.now())) {
			return neighbourInstances.getInstanceId();
		}
		
		return null;
	}

}
