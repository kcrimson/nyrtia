FROM openjdk:8u141-jdk
ENV SIMULATOR_PORT=9090
VOLUME /tmp
VOLUME /log
EXPOSE ${SIMULATOR_PORT}
ADD target/nyrtia-simulator-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Xmx128m -Djava.security.egd=file:/dev/./urandom -Djava.net.preferIPv4Stack=true -cp /app.jar io.nyrtia.simulator.Simulator -sp $SIMULATOR_PORT