package io.nyrtia.simulator;

import static io.nyrtia.Job.newJob;
import static java.lang.String.format;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.lang3.RandomUtils;
import org.eclipse.jetty.client.HttpClient;

import io.nyrtia.client.Client;
import io.nyrtia.client.ClientConfig;
import io.nyrtia.scheduler.Schedulers;
import io.nyrtia.store.MongoDBJobStoreConfig;

public class SimulatorClient {

	public static void main(String[] args) throws Exception {
		
		Options options = new Options()
			.addOption("mh", true,"mongo db host")
			.addOption("mp", true, "mongo db port")
			.addOption("sh", true, "simulator host")
			.addOption("sp", true,"simulator port");
		
		DefaultParser parser = new DefaultParser();
		CommandLine commandLine = parser.parse(options, args);
		
		MongoDBJobStoreConfig jobStoreConfig = configureJobStore(commandLine);		
		
		String simulatorHostname = commandLine.getOptionValue("sh", "localhost");
		int simulatorPort = Integer.valueOf(commandLine.getOptionValue("sp","9090"));
		
		// connect as client to cluster, start scheduling jobs at random
		Client c = Schedulers.connect(new ClientConfig().withJobStoreConfig(jobStoreConfig));
		
		HttpClient httpClient = newHttpClient();

		while(true) {
			
			String jobId = UUID.randomUUID().toString();
			// signal simulator that new job is coming
			httpClient.GET(format("http://%s:%d/scheduleJob?job_id=%s", simulatorHostname, simulatorPort,jobId));
			
			c.scheduleJob(
					newJob()
					.withJobClass(SimulatorJob.class)																																	
					.withArgs(new SimulatorJobArgs(simulatorHostname,simulatorPort,jobId))
					.withNextFireTime(LocalDateTime.now().plusSeconds(1))
					.withId(jobId)
					.build()
					);
			
			TimeUnit.MILLISECONDS.sleep(RandomUtils.nextLong(200, 500));
		}

	}

	private static HttpClient newHttpClient() throws Exception {
		HttpClient client = new HttpClient();
		client.start();
		return client;
	}

	private static MongoDBJobStoreConfig configureJobStore(CommandLine commandLine) {
		MongoDBJobStoreConfig jobStoreConfig = new MongoDBJobStoreConfig();
		if(commandLine.hasOption("mh")) {
			jobStoreConfig.withHostname(commandLine.getOptionValue("mh"));
		}
		if(commandLine.hasOption("mp")) {
			jobStoreConfig.withPort(Integer.valueOf(commandLine.getOptionValue("mp")));
		}
		return jobStoreConfig;
	}

}
