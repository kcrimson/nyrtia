package io.nyrtia.simulator;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;

public class SimulatorJob implements Runnable {
	
	private final SimulatorJobArgs args;

	public SimulatorJob(Object args) {
		super();
		this.args = (SimulatorJobArgs)args;
	}


	@Override
	public void run() {
		HttpClient client = new HttpClient();
		try {
			client.start();
			ContentResponse response = client.GET(String.format("http://%s:%d/runJob?job_id=%s", args.getHostname(), args.getPort(), args.getJobId()));
			response.getContentAsString();
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			tryStopHttpClient(client);			
		}
	}


	private void tryStopHttpClient(HttpClient client) {
		try {
			client.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
