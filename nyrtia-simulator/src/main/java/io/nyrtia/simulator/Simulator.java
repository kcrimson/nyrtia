package io.nyrtia.simulator;

import static spark.Spark.get;
import static spark.Spark.staticFileLocation;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import spark.Spark;

public class Simulator {

	private final static MetricRegistry METRIC_REGISTRY = new MetricRegistry();
	
	private final static ConcurrentHashMap<String, String> unprocessedJobs = new ConcurrentHashMap<>();

	public static void main(String[] args) throws ParseException {

		Options options = new Options().addOption("sp", true, "simulator port");

		DefaultParser parser = new DefaultParser();
		CommandLine commandLine = parser.parse(options, args);

		int simulatorPort = Integer.valueOf(commandLine.getOptionValue("sp", "9090"));

		Spark.port(simulatorPort);

		Meter misfiredJobs = METRIC_REGISTRY.meter("misfiredJobs");

		ObjectMapper mapper = new ObjectMapper();

		staticFileLocation("/static");

		get("/scheduleJob", (req, res) -> {
			String jobId = req.queryParams("job_id");

			unprocessedJobs.put(jobId, jobId);

			return "OK";
		});

		get("/runJob", (req, res) -> {
			String jobId = req.queryParams("job_id");

			unprocessedJobs.remove(jobId);

			return "OK";
		});
		
		get("/misfiredJob", (req, res) -> {
			String jobId = req.queryParams("job_id");

			unprocessedJobs.remove(jobId);
			
			misfiredJobs.mark();

			return "OK";
		});


		get("/jobs", (req, res) -> {
			res.type("application/json");
			return ImmutableMap.builder().put("size", unprocessedJobs.size()).put("misfiredJobs",misfiredJobs.getOneMinuteRate()).build();
		}, o -> mapper.writer().writeValueAsString(o));

	}

}
