package io.nyrtia.simulator;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class SimulatorJobArgs {
	
	private String hostname;
	private int port;
	private String jobId;
	
	public SimulatorJobArgs() {
		super();
	}

	public SimulatorJobArgs(String hostname, int port, String jobId) {
		super();
		this.hostname = hostname;
		this.port = port;
		this.jobId = jobId;
	}

	public String getHostname() {
		return hostname;
	}

	public int getPort() {
		return port;
	}

	public String getJobId() {
		return jobId;
	}
	
}
