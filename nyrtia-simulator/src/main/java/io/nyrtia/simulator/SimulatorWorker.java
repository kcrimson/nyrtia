package io.nyrtia.simulator;

import static java.lang.String.format;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.eclipse.jetty.client.HttpClient;

import io.nyrtia.Job;
import io.nyrtia.JobListener;
import io.nyrtia.Worker;
import io.nyrtia.config.ClusterConfig;
import io.nyrtia.scheduler.Schedulers;
import io.nyrtia.store.MongoDBJobStoreConfig;

public class SimulatorWorker {
	
	private static Worker worker;

		
	public static void main(String[] args) throws Exception {
		Options options = new Options()
			.addOption("mh", true,"mongo db host")
			.addOption("mp", true, "mongo db port")
			.addOption("sh", true, "simulator host")
			.addOption("sp", true,"simulator port");

		
		DefaultParser parser = new DefaultParser();
		CommandLine commandLine = parser.parse(options, args);
		
		
		MongoDBJobStoreConfig jobStoreConfig = new MongoDBJobStoreConfig();
		if(commandLine.hasOption("mh")) {
			jobStoreConfig.withHostname(commandLine.getOptionValue("mh"));
		}
		if(commandLine.hasOption("mp")) {
			jobStoreConfig.withPort(Integer.valueOf(commandLine.getOptionValue("mp")));
		}		
		
		String simulatorHostname = commandLine.getOptionValue("sh", "localhost");
		int simulatorPort = Integer.valueOf(commandLine.getOptionValue("sp","9090"));
		
		HttpClient httpClient = newHttpClient();

				
		worker = Schedulers.join(new ClusterConfig().withJobStore(jobStoreConfig));
		worker.addJobListener(new JobListener() {
			
			@Override
			public void jobMisfired(Job any) {
				try {
					httpClient.GET(format("http://%s:%d/misfiredJob?job_id=%s", simulatorHostname, simulatorPort,any.getId()));
				} catch (InterruptedException | ExecutionException | TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		while(true) {
			System.in.read();
		}
		
	}
	
	private static HttpClient newHttpClient() throws Exception {
		HttpClient client = new HttpClient();
		client.start();
		return client;
	}

}
