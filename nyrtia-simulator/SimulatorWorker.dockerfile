FROM openjdk:8u141-jdk
ENV MONGO_HOSTNAME=localhost
ENV MONGO_PORT=27017
ENV SIMULATOR_HOSTNAME=localhost
ENV SIMULATOR_PORT=9090
VOLUME /tmp
VOLUME /log
ADD target/nyrtia-simulator-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Xmx128m -Djava.security.egd=file:/dev/./urandom -Djava.net.preferIPv4Stack=true -cp /app.jar io.nyrtia.simulator.SimulatorWorker -mh $MONGO_HOSTNAME -mp $MONGO_PORT -sh $SIMULATOR_HOSTNAME -sp $SIMULATOR_PORT 